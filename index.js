const express = require("express");
const app = express();
const http = require("http");
const socketIO = require("socket.io");
const server = http.createServer(app);
const io = socketIO(server);

const PORT = 7050;
// set-speed {speed : }
// 1 tien 2 lui 3 trai 4 phai 13 tien trai 14 tien phai 23 lui trai 24 lui phai

let ESP;
let RAS;

let status = {
    speed: 1000,
    lighLevel: 800,
    mode: 0
};
io.on("connection", socket => {
    console.log("client connect");

    socket.on("device", data => {
        console.log("Device: ", data)
        switch (data.type) {
            case 'raspberry':
                console.log("ras")
                socket.join('RAS');

                break;
            case 'esp':
                socket.join('ESP')
                socket.emit("getInfor");
                // socket.emit("changeMode", {
                //     mode: 1
                // });
                // socket.emit("changeState", { control: 1, speed: 700 })

                // setTimeout(() => {
                //     socket.emit("changeState", { control: 7 });
                // }, 5000);
                break;

            case 'detect_mask':
                console.log("detect_mask")
                socket.join('MASK');
        }

    })
    socket.on("hello", () => {
        socket.emit("sendInfo", status);
    })

    socket.emit("sendInfo", status);

    socket.on("getInfor", data => {
        status = data;
        console.log("info ", status);
    })

    // socket.on("getInfoDevice", () => {
    //     console.log("get info device");
    //     if (ESP)
    //         ESP.emit("getInfo");
    // })


    // speed  lightLevel  mode

    // socket.on("getInfo", data => {
    //     console.log("send info device");
    //     socket.broadcast.emit("infoDevice", data);
    // })

    socket.on("turnOn", () => {
        console.log("turn on");

        socket.to("ESP").emit("turnOn");
    })

    socket.on("turnFlash", () => {
        console.log("turn flash");

        socket.to("ESP").emit("turnFlash");
    })

    socket.on("turnOff", () => {
        console.log("turn off");

        socket.to("ESP").emit("turnOff");
    })

    let temp = true;

    socket.on("no-mask", data => {
        console.log("no-mask", data)
        if (temp) {
            socket.to("RAS").emit("notification-sound", { text: "Nhựt đẹp trai quá đi nha" })
            temp = false
        }
        setTimeout(() => {
            temp = true
        }, 5000);
    })

    let isStop = false;
    // traffic sign
    let isTurnLeft = true;
    let isTurnRight = true;
    let count = 1;
    socket.on("trafficSign", data => {
        console.log("traffic sign: ", data);
        // if (data.sign === "ahead_only") {
        //     isStop = false;
        // }

        if (isStop) {
            if (count === 1) {
                count = 2
                setTimeout(() => {
                    isStop = false;
                    count = 1;
                    console.log("change isStop", isStop);
                    socket.to("ESP").emit("changeState", { control: 1, speed: 400 })
                }, 10000);
            }
        }
        console.log("isStop ", isStop)
        if (!isStop) {
            switch (data.sign) {
                case "stop":

                    // if (!RAS)
                    //     return
                    // RAS.emit("notification-sound", { text: "Nhựt đẹp trai quá đi nha" })
                    socket.to("ESP").emit("changeState", { control: 5 });

                    break;
                case 'quay_dau':
                    socket.to("ESP").emit("changeState", { control: 7 });
                    break
                case 'tam_dung':
                    socket.to("ESP").emit("changeState", { control: 5 });
                    isStop = true;
                    break;
                default:
                    // if (!isStop) {
                    //     console.log("go ahead")
                    //     ESP.emit("changeState", { control: 1, speed: 750 });
                    // }
                    break;
            }
        }
    })

    socket.on("changeSpeed", data => {  // data {speed : 1024}
        console.log("data: ", data);

        socket.to("ESP").emit("changeSpeed", data);
        status.speed = data.speed;
        socket.broadcast.emit("sendInfo", status);


    })


    socket.on("changeLightLevel", data => { // data {lightLevel : 0 -> 1024}
        console.log("data: ", data);

        socket.to("ESP").emit("changeLightLevel", data);
        status.lighLevel = data.lighLevel;
        socket.broadcast.emit("sendInfo", status);


    })



    socket.on("driver", data => {
        console.log("data: ", data);

        socket.to("ESP").emit("changeState", data);
    })

    socket.on("changeMode", data => {
        console.log("change mode: ", data);

        socket.to("ESP").emit("changeMode", data);
        status.mode = data.mode;
        socket.broadcast.emit("sendInfo", status)
        // else {
        //     data['connected'] = false;
        //     socket.broadcast.emit("sendInfo",
        //         data
        //     )
        // }

    })
    socket.on("disconnect", () => {
        console.log("client disconnect");
    })
})
server.listen(PORT, "192.168.1.103", () => {
    console.log("SERVER START");
})